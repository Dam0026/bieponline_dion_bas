function bookSearch() {
    var search = $('#search').val();
    if(isNaN(search)){
        return false;
    }else if(search.length < 14 && search.length > 9){
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + search,
            dataType: "json",
            type: 'GET',
            success: function(data) {
                for(i = 0; i < data.items.length; i++) {
                    $('#title').val(data.items[i].volumeInfo.title);
                    $('#author_id').val(data.items[i].volumeInfo.authors[i]);
                }
            }
        });
    }else{
        return false;
    }
}
$('#search').change(bookSearch);
