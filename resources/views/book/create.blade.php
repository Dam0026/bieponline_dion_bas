@extends('layouts.app')

@section('title')
	Nieuw boek toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
<div class="col-sm-12">
    <p>Scan het ISBN, de andere informatie wordt dan automatisch ingevuld.</p>
</div>
{!! Form::open(['route' => ['book.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('isbn', 'ISBN', ['class' => 'control-label']) !!}
		{!! Form::text('isbn', null, ['id' => 'search', 'class' => 'form-control', 'placeholder' => 'ISBN Nummer', 'autofocus' => 'autofocus']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('title', 'Titel', ['class' => 'control-label']) !!}
		{!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'De titel hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('author_id', 'Auteur', ['class' => 'control-label']) !!}
		{!! Form::text('author_id', null, ['class' => 'form-control', 'placeholder' => 'Auteur hier!']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection

@section('scripts')
<script src="/js/bookSearch.js" type="text/javascript"></script>
@endsection
